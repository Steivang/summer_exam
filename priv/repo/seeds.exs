# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     SummerExam.Repo.insert!(%SummerExam.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias SummerExam.Assignment.{Blog, Identity}

Identity.create_user(%{email: "Steivang@gmail.com", encrypted_password: "*****", alias: "Cato"})
Identity.create_user(%{email: "Lucky@gmail.com", encrypted_password: "****", alias: "Lucky"})
Identity.create_user(%{email: "Ohnoes@gmail.com", encrypted_password: "****", alias: "Ohnoes"})

user = Identity.get_user!(1)
country = %{country: "Norway"}
Identity.update_country(user, %{}, country)
user = Identity.get_user!(2)
country = %{country: "England"}
Identity.update_country(user, %{}, country)
user = Identity.get_user!(3)
country = %{country: "USA"}
Identity.update_country(user, %{}, country)

Blog.create_post(%{header: "test", content: "Hello world"}, 1)
Blog.create_post(%{header: "test1", content: "Hello-world"}, 2)
Blog.create_post(%{header: "test2", content: "Hello_world"}, 3)
