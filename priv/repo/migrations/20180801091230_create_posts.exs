defmodule SummerExam.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :header, :string
      add :content, :map
      timestamps()
    end
  end  
end
