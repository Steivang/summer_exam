defmodule SummerExam.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :encrypted_password, :string
      add :alias, :string
      add :avatar, :map
      add :country, :map
      timestamps()
    end
  end
end
