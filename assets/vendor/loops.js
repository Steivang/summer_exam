/* Loops */
function forLoop() {
    var output = ""
for (var index = 0; index < 10; index++) {
    output += index + "<br>";
    document.getElementById('forLoop').innerHTML = output;}}          


function forInLoop() {
    var output = ""
var x = "Cato"
for (var l in x) {
   output += x[l] + "<br>";
   document.getElementById('forInLoop').innerHTML = output;}}

function whileLoop() {
    var output = ""
var index = 0;
while (index < 4) {
    index++;
    output += "i am here" + "<br>";}
    document.getElementById('whileLoop').innerHTML = output;}


function doWhileLoop() { 
    var output = ""  
var index = 10;
do { 
  output += "running" + "<br>"; 
  index--; 
} while (index > 4);    
document.getElementById('doWhileLoop').innerHTML = output;}
    