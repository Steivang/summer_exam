# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :summer_exam,
  ecto_repos: [SummerExam.Repo]

# Configures the endpoint
config :summer_exam, SummerExamWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "A/IWZ1Fok7Ya2lzh4hlT7Hd0U7N2+PC3LhPRENRfObJPzRx72fQi2xtTXRtGSJlp",
  render_errors: [view: SummerExamWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SummerExam.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
