defmodule SummerExam.AssignmentTest do
  use SummerExam.DataCase

  alias SummerExam.Assignment

  describe "posts" do
    alias SummerExam.Assignment.Post

    @valid_attrs %{content: %{}, date: ~N[2010-04-17 14:00:00.000000], header: "some header"}
    @update_attrs %{content: %{}, date: ~N[2011-05-18 15:01:01.000000], header: "some updated header"}
    @invalid_attrs %{content: nil, date: nil, header: nil}

    def post_fixture(attrs \\ %{}) do
      {:ok, post} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Assignment.create_post()

      post
    end

    test "list_posts/0 returns all posts" do
      post = post_fixture()
      assert Assignment.list_posts() == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = post_fixture()
      assert Assignment.get_post!(post.id) == post
    end

    test "create_post/1 with valid data creates a post" do
      assert {:ok, %Post{} = post} = Assignment.create_post(@valid_attrs)
      assert post.content == %{}
      assert post.date == ~N[2010-04-17 14:00:00.000000]
      assert post.header == "some header"
    end

    test "create_post/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Assignment.create_post(@invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = post_fixture()
      assert {:ok, post} = Assignment.update_post(post, @update_attrs)
      assert %Post{} = post
      assert post.content == %{}
      assert post.date == ~N[2011-05-18 15:01:01.000000]
      assert post.header == "some updated header"
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = post_fixture()
      assert {:error, %Ecto.Changeset{}} = Assignment.update_post(post, @invalid_attrs)
      assert post == Assignment.get_post!(post.id)
    end

    test "delete_post/1 deletes the post" do
      post = post_fixture()
      assert {:ok, %Post{}} = Assignment.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> Assignment.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = post_fixture()
      assert %Ecto.Changeset{} = Assignment.change_post(post)
    end
  end
end
