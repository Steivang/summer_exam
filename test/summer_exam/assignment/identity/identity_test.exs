defmodule SummerExam.Assignment.IdentityTest do
  use SummerExam.DataCase

  alias SummerExam.Assignment.Identity

  describe "users" do
    alias SummerExam.Assignment.Identity.User

    @valid_attrs %{alias: "some alias", avatar: %{}, email: "some email", encrypted_password: "some encrypted_password"}
    @update_attrs %{alias: "some updated alias", avatar: %{}, email: "some updated email", encrypted_password: "some updated encrypted_password"}
    @invalid_attrs %{alias: nil, avatar: nil, email: nil, encrypted_password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Assignment.Identity.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Assignment.Identity.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Assignment.Identity.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Assignment.Identity.create_user(@valid_attrs)
      assert user.alias == "some alias"
      assert user.avatar == %{}
      assert user.email == "some email"
      assert user.encrypted_password == "some encrypted_password"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Assignment.Identity.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Assignment.Identity.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.alias == "some updated alias"
      assert user.avatar == %{}
      assert user.email == "some updated email"
      assert user.encrypted_password == "some updated encrypted_password"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Assignment.Identity.update_user(user, @invalid_attrs)
      assert user == Assignment.Identity.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Assignment.Identity.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Assignment.Identity.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Assignment.Identity.change_user(user)
    end
  end
end
