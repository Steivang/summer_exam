defmodule PlugTest do
    use ExUnit.Case
    use Plug.Test
    
    @opts SummerExam.Assignment.Plug.init([])

    test "returns path" do
      conn = conn(:get, "/Phoenix")

      conn = SummerExam.Assignment.Plug.call(conn, @opts)

      assert conn.page == "/Phoenix"  
    end
end    


