defmodule SummerExamWeb.Router do
  use SummerExamWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug  SummerExam.Assignment.Plug
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SummerExamWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    
    
   
    get "/git", GitController, :index
    get "/micro_patterns", MicroPatternsController, :index
  end

 
    scope "/frontend", SummerExamWeb do
    pipe_through :browser # Use the default browser stack
    get "/html", HtmlController, :index
    get "/css", CssController, :index
    get "/js", JsController, :index
   end

    scope "/backend", SummerExamWeb do
    pipe_through :browser # Use the default browser stack
    get "/elixir", ElixirController, :index
    get "/phoenix", PhoenixController, :index
    get "/ecto", EctoController, :index
   end


    scope "/blog", SummerExamWeb do
    pipe_through :browser # Use the default browser stack
    resources "/user", UserController
    resources "/post", PostController
   end
end
