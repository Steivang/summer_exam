defmodule SummerExamWeb.JsController do
    use SummerExamWeb, :controller
  
    def index(conn, _params) do
      render conn, "index.html"
    end
  end