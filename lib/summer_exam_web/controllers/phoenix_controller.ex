defmodule SummerExamWeb.PhoenixController do
    use SummerExamWeb, :controller
  
    def index(conn, _params) do
      user = SummerExam.Assignment.Identity.get_user!(1)
      render conn, "index.html", user: user
    end
  end