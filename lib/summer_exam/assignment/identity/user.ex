defmodule SummerExam.Assignment.Identity.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :alias, :string
    field :avatar, :map
    field :email, :string
    field :encrypted_password, :string
    has_many :posts,  SummerExam.Assignment.Blog.Post
    embeds_one :country, SummerExam.Assignment.Identity.Country, [on_replace: :update]
    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :encrypted_password, :alias, :avatar])
    |> validate_required([:email, :encrypted_password, :alias])
  end

  # Build Association
  def changeset_post(user, id, post) do
    SummerExam.Repo.get!(user, id)
    |> SummerExam.Repo.preload(:posts)
    |> Ecto.build_assoc(:posts, post)
  end
end
