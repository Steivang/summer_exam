defmodule SummerExam.Assignment.Identity.Country do
    use Ecto.Schema
    import Ecto.Changeset

    embedded_schema do
        field :country, :string
    
    end
    def changeset(user, attrs, country) do
        user
        |> cast(attrs, [:email, :encrypted_password, :alias, :avatar])
        |> put_embed(:country, country)
        |> validate_required([])
        
    end
end