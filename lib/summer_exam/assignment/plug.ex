defmodule SummerExam.Assignment.Plug do
    def init(options) do
      options     
    end
    def call(%Plug.Conn{:request_path => page} = conn, _default) do
      conn 
       |> Map.put_new(:page, page)
    end
end