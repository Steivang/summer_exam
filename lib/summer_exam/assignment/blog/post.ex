defmodule SummerExam.Assignment.Blog.Post do
  use Ecto.Schema
  import Ecto.Changeset


  schema "posts" do
    field :content, :string
    field :header, :string
    belongs_to :user, SummerExam.Assignment.Identity.User
    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:header, :content])
    |> validate_required([:header, :content])
  end


end
