defmodule Coin do
    def div_each([head | tail]) do
        [head / 3 | div_each(tail)]
    end

    def div_each([]) do
        []
    end
end